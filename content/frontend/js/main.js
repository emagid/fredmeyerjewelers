$(document).ready(function(){
    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }
    
    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    window.location.href = '/';
                }, 30000);
        }

        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
        });



    // PAGE LOAD
    setTimeout(function(){
        $('.logo').fadeIn(1000);
        $('.welcome').delay(2000).fadeIn(1000);
    }, 1000)

    setTimeout(function(){
        $('.popup_hero').addClass('grey');
        $('.popup_hero').fadeOut(3000);
    }, 3000);

    setTimeout(function(){
        $('.logo').css('margin-top', '0');
        $('.logo').css('height', '148px');
        $('.logo').css('padding', '0');
        $('.welcome').slideUp(100);
    }, 5000);

    setTimeout(function(){
        $('.logo').css('position', 'relative');
        $('.popup').css('height', 'auto');
        $('.popup').addClass('shadow');
        $('.home').fadeIn(2000);
    }, 6000);


    // SIDE BAR CLICK
    $('.nav').click(function(){
        $('#jQKeyboardContainer').fadeOut();
        $('.content_holder').fadeOut();
        $($(this).next('.content_holder')).fadeIn();
        $($(this).next('.content_holder')).css('display', 'flex');
    });

    $('.content_holder i').click(function(){
        $('#jQKeyboardContainer').fadeOut();
        $('.content_holder').fadeOut();
    });



});



